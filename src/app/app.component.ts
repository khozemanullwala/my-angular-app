import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Accenture -  Angular Demo Application!';
  footer = 'Designed for Demo Purpose of Docker Compose Lab!'; 
}
